import Ember from 'ember';

export default Ember.Controller.extend({
    editable: true,

    actions: {
        toggleEdit() {
            this.toggleProperty('editable');
        }
    },

});
