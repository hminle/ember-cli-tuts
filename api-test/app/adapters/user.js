import Typicode from './typicode'

export default Typicode.extend({

    pathForType() {
        return 'users'; // go to <host>/users
    }
});
